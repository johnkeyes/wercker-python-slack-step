# -*- coding: utf-8 -*-
"""Code Coverage Reporting module."""

import coverage
import io
import re


def report():
    """Coverage Report."""
    print(">>>> config_file=True <<<<")
    cov = coverage.Coverage(config_file=True)
    cov.load()
    output = io.StringIO()
    code_coverage = '%.2f%%' % (cov.report(file=output, show_missing=True))
    content = output.getvalue()
    print(content)
    last_line = content.split('\n')[-2]
    match = re.match('TOTAL +(\d+) +(\d+).*', last_line)
    matches = match.groups()

    return [
        {
            'title': 'Code Coverage',
            'value': code_coverage,
            'short': True
        },
        {
            'title': "Number of Statements",
            'value': matches[0],
            'short': True
        },
        {
            'title': "Missed",
            'value': matches[1],
            'short': True
        }
    ]
