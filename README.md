# Send Python Reports to Slack

This step sends messages to Slack from Wercker on completion of build or deploy pipelines.

## Setup

Create an Incoming Webhook for your Slack. Set default values for the channel to post to, the name, and icon. These can be overridden by the step.

Copy the Webhook URL and save it in a Wercker environment variable as `PYTHON_REPORTS_SLACK_WEBHOOK_URL`.

## Example

Send a message to the `#project` channel on Slack when builds of the `master` branch complete.

```
- jkeyes/python-reports-slack:
    branches: "master"
    channels: "#project"
```

## Step Configuration

#### Channels

`channels` — comman separated list of channels and/or users to send the message to.

```
- jkeyes/python-reports-slack:
    channels: "#general"
- jkeyes/python-reports-slack:
    channels: "#general,#staging"
```

#### Branches

`branches` — comma separated list of branches that this step is interested in. If the build or deploy is for a branch not in this list, no message is sent.

```
- jkeyes/python-reports-slack:
    branches: "master"
```

and for multiple branches:

```
- jkeyes/python-reports-slack:
    branches: "master,staging"
```

#### Ignore Branches

`ignore_branches` — a comma separated list of branches that this step is not interested in. If the build or deploy is for a branch in this list, no message is sent.

```
- jkeyes/python-reports-slack:
    ignore_branches: "master"
```

and for multiple branches:

```
- jkeyes/python-reports-slack:
    branches: "master,staging"
```

#### Username

`username` — the username who sent the message. By default this will use the `name` from the Webhook settings, but it can be set on a per-step basis.

```
- jkeyes/python-reports-slack:
    channels: "#project"
    username: "Wercker Build"
```

#### Icon URL

`icon_url` — the location of the icon for the source of the message. By default this will use the uploaded file or the emoji from the Webhook settings, but it can be set on a per-step basis.

```
- jkeyes/python-reports-slack:
    channels: "#project"
    icon_url: "https://example.com/myapp.jpg"
```

## Wercker Environment Variables

#### PYTHON_REPORTS_SLACK_WEBHOOK_URL

The Slack Incoming Webhook URL.

#### PYTHON_REPORTS_SLACK_PRINT_ENV

If True the step will print all of the environment variables into the Wercker log.
