## 1.4.2

Use specific version of pathfinder until the step fully supports the latest changes
in that utility.

## 1.4.1

Passing author name to run.py.

## 1.4.0

Bumping version number. No new changes.

## 1.3.11

Adding git commit author information.

## 1.3.10

If the ENDPOINT is specified, list it before the time and have
as `short` attachements.

## 1.3.3

Removed hardcoded `ignore` kwarg from `get_style_guide`. The step will now 
respect the value of `ignore` in the config file (if present).
