# -*- coding: utf-8 -*-
"""Build reporting package."""

import calendar
import datetime
import os

COLORS = {
    'passed': '#2ECC71',
    'failed': '#EC7063'
}

EMOJI = {
    'passed': ':tada:',
    'failed': ':sob:',
    'succeeded': ':rocket:'
}


def get_elapsed_time():
    """Return in hours and minutes how long the pipeline took to complete."""
    started = int(os.environ['WERCKER_MAIN_PIPELINE_STARTED'])
    now = datetime.datetime.utcnow()
    finished = calendar.timegm(now.utctimetuple())
    elapsed = finished - started
    return hours_minutes(elapsed)


def hours_minutes(seconds):
    """Convert seconds into hours and minutes."""
    m, s = divmod(seconds, 60)
    # parts = []
    mins = pluralize(m, 'minute')
    secs = pluralize(s, 'second')
    if mins:
        return mins + ' ' + secs
    return secs


def pluralize(number, word):
    """Pluralize the word based on the number."""
    if number:
        phrase = '%s %s' % (number, word)
        if number != 1:
            phrase += 's'
        return phrase
    return ''


def get_color(branch, result=None):
    """Return a color for the branch and result combination."""
    if result == 'failed':
        return COLORS.get(result)
    if branch != 'master':
        return '#999'
    return COLORS.get('passed')


def get_emoji(result):
    """Return an emoji for the result."""
    return EMOJI.get(result, '')
