# -*- coding: utf-8 -*-
"""Slack message for build completions."""
import os

from reporting import get_color
from reporting import get_elapsed_time
from reporting import get_emoji
from reporting import coverage_report
from reporting import flake8_report
from reporting import pydocstyle_report


def build_message(message):
    """Return the message for a build."""
    branch = os.getenv('WERCKER_GIT_BRANCH')
    result = os.getenv('WERCKER_RESULT')
    repository = os.getenv('WERCKER_GIT_REPOSITORY')
    elapsed = get_elapsed_time()
    emoji = get_emoji(result)
    color = get_color(branch, result)
    src_dirs = os.getenv('WERCKER_PYTHON_REPORTS_SLACK_SRC_DIRS')
    print("SRC_DIRS", src_dirs)
    if src_dirs:
        print("  SPLIT...")
        src_dirs = src_dirs.split(',')
    else:
        print("  DEFAULT...")
        src_dirs = ['.']
    print('Source directories: {}'.format(src_dirs))
    message['attachments'].append(
        {
            'fallback': 'Branch: {} Time: {}'.format(branch, elapsed),
            'title': '{}:{} – build {} {}'.format(repository, branch, result, emoji),
            'title_link': os.getenv('WERCKER_BUILD_URL'),
            'color': color,
            'fields': _build_fields(src_dirs)
        }
    )


def _build_fields(src_dirs):
    # run the coverage report
    commit_message = os.getenv('WERCKER_PYTHON_REPORTS_GIT_COMMIT_MESSAGE', 'n/a')
    commit_author = os.getenv('WERCKER_PYTHON_REPORTS_GIT_COMMIT_AUTHOR', 'n/a')
    elapsed = get_elapsed_time()
    fields = [
        {
            'title': "Commit",
            'value': commit_message,
            'short': False
        },
        {
            'title': "Author",
            'value': commit_author,
            'short': False
        },
        {
            'title': "Time",
            'value': elapsed,
            'short': True
        }
    ]

    if os.path.exists('.coverage'):
        fields.extend(coverage_report.report())

    errors = pydocstyle_report.report(src_dirs)
    if errors:
        fields.extend(errors)

    errors = flake8_report.report(src_dirs)
    if errors:
        fields.extend(errors)

    return fields
