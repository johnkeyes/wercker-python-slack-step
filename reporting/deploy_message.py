# -*- coding: utf-8 -*-
"""Slack message for deploy completions."""

import os

from reporting import get_color
from reporting import get_elapsed_time
from reporting import get_emoji


def deploy_message(message):
    """Return the message for a deployment."""
    branch = os.getenv('WERCKER_GIT_BRANCH')
    result = os.getenv('WERCKER_RESULT')
    if result == 'passed':
        result = 'succeeded'
    repository = os.getenv('WERCKER_GIT_REPOSITORY')
    elapsed = get_elapsed_time()
    emoji = get_emoji(result)
    color = get_color(branch, result)
    message['attachments'].append(
        {
            'fallback': 'Branch: {} Time: {}'.format(branch, elapsed),
            'title': '{}:{} – deploy {} {}'.format(repository, branch, result, emoji),
            'title_link': os.getenv('WERCKER_DEPLOY_URL'),
            'color': color,
            'fields': _build_fields()
        }
    )


def _build_fields():
    elapsed = get_elapsed_time()
    fields = []
    server = os.getenv('PYTHON_REPORTS_ENDPOINT')
    if server:
        fields.append({
            'title': "Server",
            'value': server,
            'short': True
        })

    fields.append({
        'title': "Time",
        'value': elapsed,
        'short': True
    })

    return fields
