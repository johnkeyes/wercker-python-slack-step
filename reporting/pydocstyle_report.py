# -*- coding: utf-8 -*-
"""Generate pydoc report."""

from pydocstyle.checker import check
from pydocstyle.config import ConfigurationParser


def report(src_dirs):
    """Run pydocstyle on the marty codebase."""
    conf = ConfigurationParser()
    conf.parse()
    # override the directories we are interested in
    conf._arguments = src_dirs
    errors = []
    for filename, checked_codes, ignore_decorators in \
            conf.get_files_to_check():
        # ignore Django migrations modules
        if 'migrations' in filename:
            continue
        errors.extend(check((filename,), select=checked_codes,
                            ignore_decorators=ignore_decorators))
    if errors:
        print(errors)  # these will be visible in build log
        return [{
            'title': 'pydocstyle errors',
            'value': len(errors),
            'short': True
        }]
