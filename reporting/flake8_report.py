# -*- coding: utf-8 -*-
"""Generate flake8 report."""

import pathfinder

from flake8.api import legacy as flake8


def report(src_dirs):
    """Run flake8 report."""
    py_filter = pathfinder.FnmatchFilter("*.py")
    migrations_filter = pathfinder.NotFilter(pathfinder.FnmatchFilter("*migrations*"))
    files = []
    for src in src_dirs:
        files.extend(pathfinder.find(src, filter=py_filter & migrations_filter))
    style_guide = flake8.get_style_guide()
    report = style_guide.check_files(files)

    if report.total_errors:
        return [{
            'title': 'flake8 errors',
            'value': report.total_errors,
            'short': True
        }]
