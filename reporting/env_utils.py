# -*- coding: utf-8 -*-
"""Module for environment utilities."""

import os
import sys

PROTECTED = ['PYTHON_REPORTS_SLACK_WEBHOOK_URL']


def print_env():
    """Print the all environment variables in alphabetical order."""
    keys = list(os.environ.keys())
    keys.sort()

    for key in keys:
        if key in PROTECTED or ('pass' in key.lower() or 'password' in key.lower()):
            value = '******'
        else:
            value = os.environ[key]
        print("{key}={value}".format(key=key, value=value))


def validate_env():
    """Ensure all required environment variables are set."""
    if os.getenv('PYTHON_REPORTS_SLACK_PRINT_ENV', '').lower() == 'true':
        print_env()

    if 'PYTHON_REPORTS_SLACK_WEBHOOK_URL' not in os.environ:
        # we need the webhook URL, otherwise we bail
        print('PYTHON_REPORTS_SLACK_WEBHOOK_URL environment variable not found.')
        sys.exit(0)
