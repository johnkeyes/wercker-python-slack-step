# -*- coding: utf-8 -*-
"""Script to create build and deploy reports and send them to Slack."""

import os
import requests
import sys
from reporting.env_utils import validate_env
from reporting.build_message import build_message
from reporting.deploy_message import deploy_message


def send_message():
    """Send a message to Slack."""
    webhook = os.getenv('PYTHON_REPORTS_SLACK_WEBHOOK_URL')
    username = os.getenv('WERCKER_PYTHON_REPORTS_SLACK_USERNAME')
    if not username:  # not entered in Step, default to Wercker
        username = 'Wercker'
    icon_url = os.getenv('WERCKER_PYTHON_REPORTS_SLACK_ICON_URL')
    channels = os.getenv('WERCKER_PYTHON_REPORTS_SLACK_CHANNELS')
    is_build = os.getenv('BUILD', 'false').lower() == 'true'
    is_deploy = os.getenv('DEPLOY', 'false').lower() == 'true'
    branch = os.getenv('WERCKER_GIT_BRANCH')

    # what branches are we interested in
    branches = os.getenv('WERCKER_PYTHON_REPORTS_SLACK_BRANCHES', None)
    if branches:
        branches = branches.split(',')
        if branch not in branches:
            print("Not interested in this branch: {}".format(branch))
            sys.exit(0)

    # what branches should we ignore
    ignore_branches = os.getenv('WERCKER_PYTHON_REPORTS_SLACK_IGNORE_BRANCHES', None)
    if ignore_branches:
        ignore_branches = ignore_branches.split(',')
        if branch in ignore_branches:
            print("Ignoring this branch: {}".format(branch))
            sys.exit(0)

    if channels:
        channels = channels.split(',')
    else:
        channels = []

    message = {
        'username': username,
        'attachments': []
    }
    if icon_url:
        message['icon_url'] = icon_url
    if is_build:
        build_message(message)
    elif is_deploy:
        deploy_message(message)

    for channel in channels:
        message['channel'] = channel
        _send_message(webhook, message)


def _send_message(webhook, message):
    print("Send message: {}".format(message))
    resp = requests.post(webhook, json=message)
    print("Response: {}".format(resp))


def main():
    print("Starting python-reports-slack")
    validate_env()
    send_message()


if __name__ == '__main__':
    main()
